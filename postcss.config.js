module.exports = {
    ident: 'postcss', // <= this line 
    plugins: [
        'autoprefixer',
        'postcss-preset-env',
        'postcss-cssnext',
        'cssnano'
    ],
    sourceMap: true
};