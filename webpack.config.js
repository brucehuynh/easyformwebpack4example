const webpack      = require('webpack');
const path         = require('path');
const autoprefixer = require('autoprefixer');
const precss       = require('precss');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const assetsDir       = path.resolve(__dirname, 'public/assets');
const nodeModulesDir  = path.resolve(__dirname, 'node_modules');
const plugins = require('./plugins');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const config = {
  mode: 'development',
  devtool: 'source-map',
  entry: {
    index: path.join(__dirname, "src/index.js")
  },
  output: {
    path: assetsDir,
    filename: '[name]-bundle.js'
  },
  module: {
    rules: [{
      test: /\.m?js$/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      },
      exclude: [nodeModulesDir]
    },{
      test: /\.css$/,
      use: [
        {
          loader: MiniCssExtractPlugin.loader
        },
        // {
        //   loader: 'style-loader'
        // },
        {
          loader: 'css-loader'
        },
        {
          loader: 'postcss-loader',
          options: {
            ident: 'postcss',
            plugins: (loader) => [
              require('postcss-import')({ root: loader.resourcePath }),
              require('postcss-preset-env')(),
              require('cssnano')()
            ]
          }
        }
      ]
    }, 
    {
      test: /\.(eot|woff|woff2|ttf|svg|png|jpe?g|gif)(\?\S*)?$/,
      use:{
        // loader: 'url?limit=100000@name=[name][ext]'
        loader: 'file-loader'
      }
    }
  ]
  },
  devServer: {
    publicPath: '/',
    contentBase: assetsDir,
    compress: true
  },
  plugins: [
    plugins.MiniCssExtractPlugin,
    getImplicitGlobals(),
    setNodeEnv(),
    new HtmlWebpackPlugin({
      title: 'wp4test',
      template: 'index.html',
      minify: {
        html5: true,
        preserveLineBreaks: true,
        decodeEntities: true,
      },
    })
  ]
};

function getImplicitGlobals() {
  return new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery'
  });
}

function setNodeEnv() {
  return new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('dev')
    }
  });
}

module.exports = config;