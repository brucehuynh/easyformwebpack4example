# easyFormGenerator use case example


> Use case example in a **ES6+** application with `webpack 4`.



**install dependencies:**
```bash
npm install -g webpack webpack-cli webpack-dev-server
npm install
```

**and finally bundle:**

```bash
npm run dev
```